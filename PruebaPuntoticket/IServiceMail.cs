﻿using System.Collections.Generic;
using System.IO;

namespace ExamPuntoticket
{
    public interface IServiceMail
    {
        void SendMail(IEnumerable<FileInfo> files);
    }
}